import UIKit
import YocoSDK

class ViewController: UIViewController {
  
  @IBOutlet weak var amountTextField: UITextField!
  @IBOutlet var buttonCollection: [UIButton]!
  @IBOutlet weak var scrollViewContainer: UIView!
  @IBOutlet weak var segCtrlTippingConfig: UISegmentedControl!
  
  
  private var printerConfig: PrinterConfig = PrinterConfig()
  private var transactionId: String? = nil
  var tippingConfig: TippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
  var receiptNumber:String?
  var tippingSetting: TippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
  
  func getRefundParams() -> RefundParameters {
    return RefundParameters(amountInCents: 200, receiptDelegate: self, userInfo: [
      "user" : "info"
    ], staffMember: YocoStaff(staffNumber: "1234",
                              name: "Joe Bloggs"), refundCompleteNotifier: { paymentResult in
      // Get notified immediately when the transaction is complete.
    })
  }
  func getPaymentParams() -> PaymentParameters {
    return PaymentParameters(receiptDelegate: self,
                             userInfo: [
                              "user" : "info"
                             ],
                             staffMember: YocoStaff(staffNumber: "1234",
                                                    name: "Joe Bloggs"),
                             receiptNumber: self.receiptNumber,
                             billId: "test-bill-id",
                             note: "test note",
                             transactionCompleteNotifier: { paymentResult in
      // Get notified immediately when the transaction is complete.
        self.transactionId = paymentResult.transactionID
    })
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // If something happened during a transaction, attempt to get the transaction status and display the result to the user.
    if let transactionID = UserDefaults.standard.value(forKey: "transactionInProgress") as? String {
      Yoco.getPaymentResult(transactionID: transactionID) { paymentResult, error in
        if let paymentResult = paymentResult {
          Yoco.showPaymentResult(paymentResult, paymentParameters: self.getPaymentParams()) { paymentResult in
            UserDefaults.standard.set(nil, forKey: "transactionInProgress")
          }
        }
      }
    }
    
    applyTheme()
  }
  
  func applyTheme() {
    for button in buttonCollection {
      button.layer.cornerRadius = 3
    }
    
    let grey = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1)
    
    scrollViewContainer.layer.cornerRadius = 8
    scrollViewContainer.layer.shadowColor = UIColor.black.cgColor
    scrollViewContainer.layer.shadowOpacity = 0.3
    scrollViewContainer.layer.shadowOffset = .zero
    scrollViewContainer.layer.shadowRadius = 5
    scrollViewContainer.backgroundColor = UIDevice.current.userInterfaceIdiom == .phone ? grey : UIColor.white
    view.backgroundColor = UIDevice.current.userInterfaceIdiom == .phone ? UIColor.white : grey
  }
  
  // MARK: IBActions
  @IBAction func autoPrintChanged(_ sender: UISwitch) {
    let printerConfig = PrinterConfig(autoPrint: sender.isOn)
    self.printerConfig = printerConfig
  }
  
  @IBAction func tippingChanged(_ sender: UISegmentedControl) {
    var message = ""
    switch(sender.selectedSegmentIndex) {
    case 1:
      tippingSetting = TippingConfig.ASK_FOR_TIP_ON_CARD_MACHINE
      message = "Ask for tip on card machine"
    case 2:
      tippingSetting = TippingConfig.INCLUDE_TIP_IN_AMOUNT(tipInCents: 200)
      message = "Use config from Yoco System"
    default:
      tippingSetting = TippingConfig.DO_NOT_ASK_FOR_TIP
      message = "Never ask for tip on card machine"
    }
    
    self.showToast(
      title: "Tipping",
      message: message
    )
  }
  
  @IBAction func startCardPayment(_ sender: UIButton) {
    startTransaction(type: .card)
  }
  
  @IBAction func startQRPayment(_ sender: UIButton) {
    startTransaction(type: .qr)
  }
  
  @IBAction func refundPressed(_ sender: UIButton) {
    if let transactionID = UserDefaults.standard.value(forKey: "lastTransactionID") as? String {
      Yoco.refund(transactionID: transactionID, parameters: self.getRefundParams())
    } else {
      let alert = UIAlertController(title: "Error", message: "No transaction has been done yet to refund, please do a transaction first", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        alert.dismiss(animated: true)
      })
      
      present(alert, animated: true)
    }
  }
  @IBAction func lookupByReceiptNumberPressed(_ sender: UIButton) {
    if let receiptNumber = self.receiptNumber {
      Yoco.getIntegratorTransactions(receiptNumber: receiptNumber) { (result, error) in
        if let result = result {
          let alert = UIAlertController(title: "Transactions Found", message: "\(result.count) transaction(s) found", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
            alert.dismiss(animated: true)
          })
          
          self.present(alert, animated: true)
        } else {
          let alert = UIAlertController(title: "Error", message: "Error looking up transaction \(String(describing: error?.localizedDescription))", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
            alert.dismiss(animated: true)
          })
          
          self.present(alert, animated: true)
        }
      }
    } else {
      let alert = UIAlertController(title: "Error", message: "No transaction has been done yet to lookup, please do a transaction first", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        alert.dismiss(animated: true)
      })
      
      present(alert, animated: true)
    }
  }
  @IBAction func printWithLookupPressed(_ sender: UIButton) {
    if let transactionId = self.transactionId {
      let merchantInfo = MerchantInfo(
        merchantId: "merchant-id",
        merchantName: "Boring Trends",
        phone: "0123456789",
        address: "Underground Business 1, ZA"
      )
      let printRequest = PrintRequest(
        printRequestId: transactionId,
        clientTransactionId: transactionId,
        printerConfig: self.printerConfig,
        receiptInfo: nil,
        merchantInfo: merchantInfo,
        transactionType: YocoTransactionType.GOODS,
        receiptType: ReceiptCopyType.BOTH,
        signature: nil,
        metadata: ["Meta" : "Data"]
      )
      Yoco.print(rootViewController: self, printRequest: printRequest) { result in
        
      }
    } else {
      let alert = UIAlertController(title: "Error", message: "No transaction has been done yet to print, please do a transaction first", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
        alert.dismiss(animated: true)
      })
      
      present(alert, animated: true)
    }
  }
  @IBAction func pairButtonPressed(_ sender: UIButton) {
    Yoco.pairTerminal()
  }
  
  @IBAction func logoutButtonPressed(_ sender: UIButton) {
    Yoco.logout()
    
    let alert = UIAlertController(title: "Success", message: "You have logged out successfully.", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
      alert.dismiss(animated: true)
    })
    
    present(alert, animated: true)
  }
  
  // MARK: Transaction functions
  
  func startTransaction(type: YocoPaymentType) {
    if let amount = amountTextField.text, !amount.isEmpty {
      let cents = Double(amount)! * 100
      let money = UInt64(cents.rounded())
        self.receiptNumber = UUID().uuidString
      let transactionID = Yoco.charge(money,
                                      paymentType: type,
                                      currency: .zar,
                                      tippingConfig: tippingConfig,
                                      parameters: self.getPaymentParams()) { paymentResult in
        // Do something once the payment is complete and the YocoSDK has dismissed
        UserDefaults.standard.set(nil, forKey: "transactionInProgress")
      }
      
      UserDefaults.standard.set(transactionID, forKey: "transactionInProgress")
      UserDefaults.standard.set(transactionID, forKey: "lastTransactionID")
    }
  }
  @IBAction func tippingConfigurationChanged(_ sender: Any) {
    switch segCtrlTippingConfig.selectedSegmentIndex {
    case 0:
      tippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
      break
    case 1:
      tippingConfig = TippingConfig.ASK_FOR_TIP_ON_CARD_MACHINE
      break
    case 2:
      if let amount = amountTextField.text, !amount.isEmpty {
        let cents = Double(amount)! * 100
        let money = Int32(cents.rounded() * 0.1)
        
        tippingConfig = TippingConfig.INCLUDE_TIP_IN_AMOUNT(tipInCents: money)
      }
      break
    default:
      tippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
      break
    }
  }
  
  // MARK: Receipt functions
  
  func validate(phoneNumber: String?) -> Bool {
    
    if let number = phoneNumber, number.count > 0 {
      return true
    }
    
    return false
  }
  
  func validate(emailAddress: String?) -> Bool {
    
    if let email = emailAddress, email.count > 0 {
      return true
    }
    
    return false
  }
}

extension ViewController: YocoReceiptDelegate {
  
  func supportedReceiptTypes() -> [SupportedReceiptType] {
    return [.email, .sms, .print]
  }
  
  // MARK: - optional delegate methods
  func sendSMSReceipt(phoneNumber: String, paymentResult: PaymentResult, progress: @escaping UpdateReceiptProgress) {
    if validate(phoneNumber: phoneNumber) {
      progress(.complete())
    } else {
      progress(.error(message: "Phone number is invalid"))
    }
  }
  
  func sendEmailReceipt(address: String, paymentResult: PaymentResult, progress: @escaping UpdateReceiptProgress) {
    progress(.inProgress())
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
      
      if self.validate(emailAddress: address) {
        progress(.complete())
      } else {
        progress(.error(message: "Email is invalid"))
      }
    }
  }
  
  func printReceipt(canPrintOnTerminal: Bool, message: String?, paymentResult: PaymentResult, progress: @escaping UpdateReceiptProgress) {
    if(canPrintOnTerminal) {
      if let receiptInfo = paymentResult.receiptInfo {
        let merchantInfo = MerchantInfo(
          merchantId: "merchant-id",
          merchantName: "Boring Trends",
          phone: "0123456789",
          address: "Underground Business 1, ZA"
        )
        let printRequest = PrintRequest(
          printRequestId: paymentResult.transactionID,
          clientTransactionId: "",
          printerConfig: self.printerConfig,
          receiptInfo: receiptInfo,
          merchantInfo: merchantInfo,
          transactionType: YocoTransactionType.GOODS,
          receiptType: ReceiptCopyType.BOTH,
          signature: nil,
          metadata: ["Meta" : "Data"]
        )
        
        progress(.inProgress(message: "Printing..."))
          Yoco.isPrinterAvailable { available, error in
              
          }
        Yoco.printReceipt(printRequest: printRequest) { printResult in
          if (printResult?.completed == true) {
            progress(.complete())
          } else {
            progress(.error(message: printResult?.errorMessage ?? "Print failed"))
          }
        }
      } else {
        progress(.error(message: "Payment request is missing receiptInfo"))
      }
    } else {
      if let _error = message {
        progress(.error(message: _error))
      } else {
        progress(.inProgress(message: "Fake 3rd party printer: Initialising..."))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
          
          progress(.inProgress(message: "Fake 3rd party printer: Printing..."))
          
          DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            progress(.complete())
          }
        }
      }
    }
  }
  private func showToast(title:String?, message:String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
    
    if let popoverController = alert.popoverPresentationController {
      popoverController.sourceView = self.view
      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
    }
    
    self.present(alert, animated: true)
    let seconds = 1.5
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
      alert.dismiss(animated: true)
    }
  }
}
