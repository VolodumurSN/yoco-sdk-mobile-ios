# YocoSDK

[![Version](https://img.shields.io/cocoapods/v/YocoSDK.svg?style=flat)](https://cocoapods.org/pods/YocoSDK)
[![License](https://img.shields.io/cocoapods/l/YocoSDK.svg?style=flat)](https://gitlab.com/yoco-public/yoco-sdk-mobile-ios/-/blob/master/LICENSE)
[![Platform](https://img.shields.io/cocoapods/p/YocoSDK.svg?style=flat)](https://cocoapods.org/pods/YocoSDK)

# Why fork it?

Well, for our react-native project we need minimum deployment target of iOS 13.0, so I bumped it.

I decided to publish it so that others with a similar use-case might profit from it, too.

## Requirements

iOS 13.0 and above, and a secret key acquired from Yoco.

## Installation

Please see our installation guide at https://developer.yoco.com/

## Author

Jacques Questiaux, support@yoco.com

## License

YocoSDK is available under the MIT license. See the LICENSE file for more info.
