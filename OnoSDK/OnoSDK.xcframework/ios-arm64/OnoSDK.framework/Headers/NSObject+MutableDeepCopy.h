//
//  NSDictionary+DeepMutable.h
//  YocoSDK-UI
//
//  Created by Andrew Snowden on 2018/04/10.
//  Copyright © 2018 Yoco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (MutableDeepCopy)
- (id)mutableDeepCopy;
@end
